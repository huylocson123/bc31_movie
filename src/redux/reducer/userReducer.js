import { useReducer } from "react";

let initialState = {
    userInfor: {},
};
export let userReducer = (state = initialState, action) => {
    switch (action.type) {
        case "LOGIN": {
            state.userInfor = action.payload;
            return { ...state }
        }
        default: return state;
    }
}