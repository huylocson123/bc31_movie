const USER = "USER";

export let localStorageServ = {
    user: {
        set: function (dataUser) {
            let dataJson = JSON.stringify(dataUser);
            localStorage.setItem(USER, dataJson);
        },
    },
};