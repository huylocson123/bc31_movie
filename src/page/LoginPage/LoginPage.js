import { Form, Input, message } from 'antd';
import React from 'react'
import { localStorageServ } from '../../services/localStorageServices';
import { userServices } from '../../services/userServices/userServices';
import { useNavigate } from "react-router-dom";

export default function LoginPage() {
    let history = useNavigate();
    const onFinish = (values) => {
        console.log('Success:', values);
        userServices.postLogin(values).then((res) => {
            message.success("Đăng nhập thành công");
            localStorageServ.user.set(res.data.content);
            setTimeout(() => {
                //  chuyen trang sau khi dang nhap thanh cong
                history("/");
            }, 1000);

            // console.log(res);
        }).catch((err) => {
            message.error(err.response.data.content);
            // console.log(err.response.data);
        });
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div className='bg-red-400  h-screen w-screen p-10'>
            <div className='container mx-auto bg-white rounded-xl p-10'>
                <Form
                    layout='vertical'
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item

                        label="Tài khoản"
                        name="taiKhoan"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Mật khẩu"
                        name="matKhau"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <div className='flex justify-center'>
                        <button className='rounded px-5 py-2 text-white bg-red-500'>
                            Đăng nhập
                        </button>
                    </div>
                </Form>
            </div>
        </div>
    );
}

