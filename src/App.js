import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Layout from './HOC/Layout';
import DetailPage from './page/DetailPage/DetailPage';
import HomePage from './page/HomePage/HomePage';
import LoginPage from './page/LoginPage/LoginPage';

function App() {
  return (
    <div className="">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Layout Component={HomePage} />} />
          <Route path='/login' element={<LoginPage />} />
          <Route path='/detail' element={<Layout Component={DetailPage} />} />
        </Routes>
      </BrowserRouter>

    </div>
  );
}

export default App;
